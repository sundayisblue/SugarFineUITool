﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SugarFineUI.CodeGenerator._default" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>首页</title>
  
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager ID="PageManager1" AutoSizePanelID="regionPanel" runat="server" />
        <f:Panel ID="regionPanel" Layout="Region" CssClass="mainpanel" ShowBorder="false" ShowHeader="false" runat="server">
            <Items>
              
                <f:Panel ID="leftPanel" CssClass="leftregion bgpanel" 
                    EnableCollapse="True" Width="200px" RegionSplit="true" RegionSplitIcon="false" RegionSplitWidth="3px"
                    ShowHeader="false" Title="功能列表" Layout="Fit" RegionPosition="Left"   runat="server">
                    <Items>
                        <f:Tree runat="server" ID="panelLeftRegion" RegionPosition="Left" RegionSplit="true" EnableCollapse="False" 
                            Width="250px" Title="功能列表" ShowBorder="true" ShowHeader="true" BodyPadding="5px"  >
                            <Nodes>
                                <f:TreeNode Text="代码生成" Expanded="true" NodeID="CodeGenerator" >
                                    <f:TreeNode Text="FineuiPro" NodeID="FineuiPro" NavigateUrl="FineuiPro/SetFineuiProCode.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="SqlSugar" NodeID="SqlSugar" NavigateUrl="SqlSugar/SetCode.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <%--<f:TreeNode Text="数据库连接" NodeID="DbConfig" NavigateUrl="DbConfig.aspx" Expanded="true"  EnableClickEvent="true"/>--%>
                                </f:TreeNode> 

                                <f:TreeNode Text="实体反射" Expanded="True" NodeID="ModelFirst" >
                                    <f:TreeNode Text="反射生成FineuiPro" NodeID="Model_FineuiPro" NavigateUrl="ModelFirst/ModelToFineuiProCode.aspx" Expanded="true" EnableClickEvent="true"/>
                                    <f:TreeNode Text="反射获取字段代码" NodeID="ShowFields" NavigateUrl="ModelFirst/ShowFields.aspx" Expanded="true" EnableClickEvent="true"/>
                               </f:TreeNode> 

                                <f:TreeNode Text="字符串操作" Expanded="false" NodeID="code"  >
                                    <f:TreeNode Text="拼接" NodeID="Mosaic" NavigateUrl="/tool/Mosaic.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="取2个字符串中间的值" NodeID="BetweenStr" NavigateUrl="/tool/BetweenStr.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="调换" NodeID="Transformation" NavigateUrl="/tool/Transformation.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="替换" NodeID="replacecode" NavigateUrl="/tool/replacecode.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="转单行" NodeID="MultiLine" NavigateUrl="/tool/MultiLine.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="转多行" NodeID="StringToList" NavigateUrl="/tool/StringToList.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="HtmlAgilityPack" NodeID="xpath" NavigateUrl="/tool/xpath.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="Fiddler代码生成" NodeID="FiddlerRaw" NavigateUrl="/tool/FiddlerRaw.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="生成Post代码" NodeID="post_Helper" NavigateUrl="/tool/post_Helper.aspx" Expanded="true"  EnableClickEvent="true"/>
                                </f:TreeNode>

                                <f:TreeNode Text="工具网站" Expanded="false" NodeID="webTool" >
                                    <f:TreeNode Text="脚本之家" NodeID="jb51" NavigateUrl="http://tools.jb51.net/code/zishutongji" />
                                    <f:TreeNode Text="工具吧" NodeID="atool8" NavigateUrl="https://www.atool8.com"  />
                                    <f:TreeNode Text="在线工具" NodeID="tool_lu" NavigateUrl="https://tool.lu/" />
                                </f:TreeNode>  
                                
                                <f:TreeNode Text="其他设置" Expanded="false" NodeID="other" >
                                    <f:TreeNode Text="主题" NodeID="theme" NavigateUrl="FineuiPro/theme.aspx" Expanded="true" EnableClickEvent="true"/>
                                </f:TreeNode>
                            </Nodes>
                        </f:Tree>
                    </Items>
                </f:Panel>
                <f:TabStrip ID="mainTabStrip" CssClass="centerregion" RegionPosition="Center" EnableTabCloseMenu="true" ShowBorder="false" runat="server">
                    <Tabs>
                        <f:Tab  Title="首页" Icon="House" runat="server"   >
                            <Items>
                            <f:Label runat="server" EncodeText="false" Text="&nbsp;&nbsp;&nbsp;欢迎使用"/>
                            </Items>
                        </f:Tab>
                    </Tabs>
                    <Tools>
                        <f:Tool runat="server" EnablePostBack="false" IconFont="_Refresh" CssClass="tabtool" ToolTip="刷新本页" ID="toolRefresh">
                            <Listeners>
                                <f:Listener Event="click" Handler="onToolRefreshClick" />
                            </Listeners>
                        </f:Tool>
                        <f:Tool runat="server" EnablePostBack="false" IconFont="_Maximize" CssClass="tabtool" ToolTip="最大化" ID="toolMaximize">
                            <Listeners>
                                <f:Listener Event="click" Handler="onToolMaximizeClick" />
                            </Listeners>
                        </f:Tool>
                    </Tools>
                </f:TabStrip>
            </Items>
        </f:Panel>
        <f:Window ID="Window1" runat="server" IsModal="true" Hidden="true" EnableIFrame="true"
            EnableResize="true" EnableMaximize="true" IFrameUrl="edit.aspx" Width="800px"
            Height="500px">
        </f:Window>
       
    </form>
    <script>
  
        var editWindow = '<%= Window1.ClientID %>';
        var mainTabStripClientID = '<%= mainTabStrip.ClientID %>';
       
        var leftPanelClientID = '<%= leftPanel.ClientID %>';


        // 点击标题栏工具图标 - 刷新
        function onToolRefreshClick(event) {
            var mainTabStrip = F(mainTabStripClientID);

            var activeTab = mainTabStrip.getActiveTab();
            if (activeTab.iframe) {
                var iframeWnd = activeTab.getIFrameWindow();
                iframeWnd.location.reload();
            }
        }

        // 点击标题栏工具图标 - 最大化
        function onToolMaximizeClick() {
            var leftPanel = F(leftPanelClientID);
            var currentTool = this;
            F.noAnimation(function () {
                if (currentTool.iconFont === 'f-iconfont-maximize') {
                    currentTool.setIconFont('f-iconfont-restore');
                    leftPanel.collapse();
                } else {
                    currentTool.setIconFont('f-iconfont-maximize');
                    leftPanel.expand();
                }
            });
        }

        //菜单样式
        function setMenuStyle(styleVal) {
            //accordion
            F.cookie('MenuStyle', styleVal, {
                expires: 100  // 单位：天
            });

            top.window.location.reload();
        }


        // 添加示例标签页
        // id： 选项卡ID
        // iframeUrl: 选项卡IFrame地址 
        // title： 选项卡标题
        // icon： 选项卡图标
        // createToolbar： 创建选项卡前的回调函数（接受tabOptions参数）
        // refreshWhenExist： 添加选项卡时，如果选项卡已经存在，是否刷新内部IFrame
        // iconFont： 选项卡图标字体
        function addExampleTab(tabOptions) {

            if (typeof (tabOptions) === 'string') {
                tabOptions = {
                    id: arguments[0],
                    iframeUrl: arguments[1],
                    title: arguments[2],
                    icon: arguments[3],
                    createToolbar: arguments[4],
                    refreshWhenExist: arguments[5],
                    iconFont: arguments[6]
                };
            }

            F.addMainTab(F(mainTabStripClientID), tabOptions);
        }


        // 移除选中标签页
        function removeActiveTab() {
            var mainTabStrip = F(mainTabStripClientID);

            var activeTab = mainTabStrip.getActiveTab();
            activeTab.hide();
        }


        F.ready(function () {

            var mainTabStrip = F(mainTabStripClientID);
            var leftPanel = F(leftPanelClientID);
            var treeMenu = leftPanel.getItem(0);

            // 初始化主框架中的树(或者Accordion+Tree)和选项卡互动，以及地址栏的更新
            // treeMenu： 主框架中的树控件实例，或者内嵌树控件的手风琴控件实例
            // mainTabStrip： 选项卡实例
            // updateHash: 切换Tab时，是否更新地址栏Hash值（默认值：true）
            // refreshWhenExist： 添加选项卡时，如果选项卡已经存在，是否刷新内部IFrame（默认值：false）
            // refreshWhenTabChange: 切换选项卡时，是否刷新内部IFrame（默认值：false）
            // maxTabCount: 最大允许打开的选项卡数量
            // maxTabMessage: 超过最大允许打开选项卡数量时的提示信息
            F.initTreeTabStrip(treeMenu, mainTabStrip, {
                //maxTabCount: 10,
                //maxTabMessage: '请先关闭一些选项卡（最多允许打开 10 个）！'
                updateHash:false
            });
        });

    </script>
</body>
</html>
