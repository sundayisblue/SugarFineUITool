﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetCode.aspx.cs" Inherits="SugarFineUI.CodeGenerator.SqlSugar.SetCode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SqlSugar实体生成</title>
</head>
<body>
<form id="form1" runat="server">
    <f:PageManager runat="server" AutoSizePanelID="Panel1" FormLabelWidth="135px" />
    <f:Panel ID="Panel1" CssClass="blockpanel" Margin="5px" runat="server" ShowBorder="false" ShowHeader="false" Layout="Region">
        <items>
            <f:Form ID="form_Edit" ShowBorder="false" ShowHeader="false" runat="server" AutoScroll="true"
                    EnableCollapse="true" BodyPadding="15px 15px" LabelWidth="200px" IsFluid="true"  >
                <rows>
                    <f:FormRow runat="server" >
                        <Items>
                            <f:Label runat="server" Label="说明"  Text="<font color='red'>默认生成代码存放在c盘下，请自行替换到项目中。</font>"  EncodeText="False"/>
                        </Items>
                    </f:FormRow>
                    <f:FormRow runat="server" >
                        <Items>
                            <f:TextBox runat="server" ID="txb_path" Label="代码存放位置" Text="C:\Code\Enties" Required="True" ShowRedStar="True"/>
                        </Items>
                    </f:FormRow>
                    <f:FormRow runat="server" >
                        <Items>
                            <f:TextBox runat="server" ID="txb_nameSpace" Label="Sugar实体空间" Text="SugarFineUI.Enties" Required="True" ShowRedStar="True"/>
                        </Items>
                    </f:FormRow>
                    <f:FormRow runat="server" >
                        <Items>
                            <f:Button ID="Button_save" runat="server" ValidateForms="form_Edit" Text="生成代码" Icon="SystemSaveNew"
                                      OnClick="Button_save_OnClick"  /><%--OnClientClick="if(!GetTableInfos())return ;"--%>
                        </Items>
                    </f:FormRow>
                </rows>
            </f:Form>
            
           <%-- <f:Panel runat="server" ID="panelRightRegion" RegionPosition="Right" RegionSplit="true" EnableCollapse="true" EnableIFrame="True" IFrameUrl="/Public/DbConfig.aspx"
                     Width="200px" Title="数据库" Layout="VBox" ShowBorder="true" ShowHeader="true" BodyPadding="5px" IconFont="_PullRight">
            </f:Panel>--%>
        </items>
    </f:Panel>
    <%--<f:HiddenField runat="server" ID="hd_tables" />--%>
</form>
</body>
</html>
<script>
    var hd_tables;

    <%--F.ready(function () {
        hd_tables = F('<%=hd_tables.ClientID%>');
    });--%>
    function GetTableInfos() {
        var error = "";

        //获取表名称
        var tables = $('#Panel1_panelRightRegion iframe')[0].contentWindow.GetTables();
        if (tables.length == 0) {
            error += "请在右侧[数据库]中，选中目标表！";
        }

        if (error.length > 0) {
            F.alert(error);
            return false;
        }

        //取表名称值
        hd_tables.setValue(tables);

        if (error.length > 0) {
            F.alert(error);
            return false;
        }
        return true;
    }
</script>