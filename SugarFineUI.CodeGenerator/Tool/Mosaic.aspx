﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Mosaic.aspx.cs" Inherits="SugarFineUI.CodeGenerator.Tool.Mosaic" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>拼接代码</title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="form_Edit" />

        <f:Form ID="form_Edit" ShowBorder="True" ShowHeader="False" Title=" " runat="server" AutoScroll="true"
                EnableCollapse="false" BodyPadding="15px 15px">
            <Rows>
                <f:FormRow runat="server" ColumnWidth="50% 50%" >
                    <Items>
                        <f:TextArea runat="server" ID="txb_Expression" Label="表达式"   Required="True" ShowRedStar="True"  />
                       
                    </Items>
                </f:FormRow>

                

                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_old" Height="300px" Required="True" ShowRedStar="True" Label="批量内容"/>
                    </Items>
                </f:FormRow>


                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_new"  Height="300px" Readonly="True" Label="新内容"/>
                    </Items>
                </f:FormRow>

            </Rows>

            <Toolbars>
                <f:Toolbar runat="server" Position="Top">
                    <Items>
                        <f:Button runat="server" ValidateForms="form_Edit" ID="btn_showCode" Text="拼接代码" Icon="Html" OnClick="btn_showCode_OnClick" />
                        <f:ToolbarSeparator runat="server" />
                       <f:ToolbarText runat="server" Text="字符串:Name string, 根据表达式public {1} {0} {{get; set;}}  拼接成 public string Name {get; set;}。<font color='red'>注意:如果输出 “{” 或 “}” 时需要转义，既 “{{” 或 “}}” </font>"/>
                    </Items>
                </f:Toolbar>
            </Toolbars>
        </f:Form>
    </form>
</body>
</html>
