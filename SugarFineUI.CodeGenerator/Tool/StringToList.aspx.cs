﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUIPro;


namespace SugarFineUI.CodeGenerator.Tool
{
    public partial class StringToList :FineUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btn_showCode_OnClick(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            string[] sArray = Regex.Split(txb_string.Text, txb_Split.Text, RegexOptions.IgnoreCase);
        
            for (int i = 0; i < sArray.Length; i++)
            {
                sb.AppendLine(sArray[i] + txb_Semicolon.Text);
            }

            txa_List.Text = sb.ToString();
        }
    }
}