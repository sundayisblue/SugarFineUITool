﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

 

namespace FineUIPro
{
    public static partial class FineuiProExtensions
    {
            /// <summary>
            /// 显示信息并跳转到指定页面
            /// </summary>
            /// <param name="alert"></param>
            /// <param name="message"></param>
            /// <param name="url"></param>
            public static void AlertInforAndRedirect(this FineUIPro.Alert alert, string message, string url)
            {
                FineUIPro.Alert.ShowInParent(message, "提示对话框", MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference(string.Format("self.location='{0}' ", url)));
            }

            /// <summary>
            /// 提示信息(弹层用)
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="isPostBackReference">是否回发刷新页面</param>
            public static void AlertInfor(this FineUIPro.Alert alert, string message, bool isPostBackReference)
            {
                if (isPostBackReference)
                {
                    //弹层页用,结合window的Onclose事件使用
                    FineUIPro.Alert.ShowInParent(message, "提示对话框", MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());

                    //弹层页用
                    //Alert.ShowInParent(message, "提示对话框", MessageBoxIcon.Information, ActiveWindow.GetHideRefreshReference());

                    //单页 并刷新当前页
                    //Alert.ShowInParent(message, "提示对话框", MessageBoxIcon.Information, "window.location.reload()");
                }
                else
                    FineUIPro.Alert.ShowInParent(message, MessageBoxIcon.Information);

            }



            /// <summary>
            /// 提示信息（单页用 并刷新当前页）
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="isPostBackReference">是否回发刷新页面</param>
            public static void AlertInforBySingerPage(this FineUIPro.Alert alert, string message, bool isPostBackReference)
            {
                if (isPostBackReference)
                {
                    //单页 并刷新当前页
                    FineUIPro.Alert.ShowInParent(message, "提示对话框", MessageBoxIcon.Information, "window.location.reload()");
                }
                else
                    FineUIPro.Alert.ShowInParent(message, MessageBoxIcon.Information);

            }

            /// <summary>
            /// 报错信息
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="isPostBackReference">是否回发刷新页面</param>
            public static void AlertError(this FineUIPro.Alert alert, string message, bool isPostBackReference)
            {
                if (isPostBackReference)
                    FineUIPro.Alert.ShowInParent(message, "提示对话框", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                else
                    FineUIPro.Alert.ShowInParent(message, MessageBoxIcon.Error);
            }

            /// <summary>
            /// 疑问信息
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="isPostBackReference">是否回发刷新页面</param>
            public static void AlertQuestion(this FineUIPro.Alert alert, string message, bool isPostBackReference)
            {
                if (isPostBackReference)
                    FineUIPro.Alert.ShowInParent(message, "提示对话框", MessageBoxIcon.Question, ActiveWindow.GetHidePostBackReference());
                else
                    FineUIPro.Alert.ShowInParent(message, MessageBoxIcon.Question);
            }

            /// <summary>
            /// 警告信息
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="isPostBackReference">是否回发刷新页面</param>
            public static void AlertWarning(this FineUIPro.Alert alert, string message, bool isPostBackReference)
            {
                if (isPostBackReference)
                    FineUIPro.Alert.ShowInParent(message, "提示对话框", MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                else
                    FineUIPro.Alert.ShowInParent(message, MessageBoxIcon.Warning);
            }

            /// <summary>
            /// 子页面关闭并在父业面跳转
            /// </summary>
            /// <param name="url"></param>
            public static void ParentRedirect(this FineUIPro.Alert alert, string url)
            {
                PageContext.RegisterStartupScript(ActiveWindow.GetHidePostBackReference(url));
            }
        }
    }

