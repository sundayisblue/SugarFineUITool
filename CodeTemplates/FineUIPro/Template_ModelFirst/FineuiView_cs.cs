﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SugarFineUI.CodeTemplates.FineUIPro.Template_ModelFirst
{
    public class FineuiView_cs : FineuiBase
    {
        public static string GetCode(TableModel tb)
        {
            var list = tb.Columns.Where(p => p.IsPrimarykey == false).ToList();

            StringBuilder sb = new StringBuilder();//获取页面加载值
            for (int i = 0; i < list.Count; i++)
            {
                sb.AppendLine(string.Format("                        F_{0}.Text = mo.{0}.ToString(); ", list[i].DbColumnName));
            }

            string code = string.Format(@"
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SqlSugar ;
using FineUIPro;

namespace {0}.{1}{2}
{{
    public partial class View : {3}
    {{  
        protected void Page_Load(object sender, EventArgs e)
        {{
            if (!IsPostBack)
            {{    
                if (Request[""id""] != null) //修改
                {{
                    try
                    {{
                        {5}.{2} mo = DBServices.DB_Base.GetModel<{5}.{2}>(int.Parse(Request[""id""]));       
{4}                    
                    }}
                    catch (Exception ex)
                    {{
                        Response.End();
                    }}
                }}
            }}
        }}

        
    }}
}}

", tb.NamespaceStr,
   GetNamespace2Str(tb.Namespace2Str),
   tb.TableName,
   tb.ClassnameStr, 
   sb.ToString(),
   tb.ModelName ) ;


            return code ;
        }

       
    }
}
