﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SugarFineUI.CodeTemplates.FineUIPro.Template
{
    public class FineuiAdd_cs:FineuiBase
    {
        public static string GetCode(TableModel tb)
        {
            //todo 改成多数据库的形式

            var list = tb.Columns.Where(p => p.IsPrimarykey == false).ToList();

            StringBuilder sb = new StringBuilder();//获取页面加载值
            StringBuilder sb2 = new StringBuilder();//赋值
            StringBuilder sb3 = new StringBuilder();//int验证
            StringBuilder sb4 = new StringBuilder();//update要更新的字段
            for (int i = 0; i < list.Count; i++)
            {
                switch (list[i].DataType.ToLower())
                {
                    case "bit":
                        sb.AppendLine(string.Format("                        F_{0}.Checked = mo.{0}; ", list[i].DbColumnName));
                        sb2.AppendLine(string.Format("            mo.{0}=F_{0}.Checked; ", list[i].DbColumnName));
                        break;
                    case "datetime":
                        sb.AppendLine(string.Format("                        F_{0}.SelectedDate = mo.{0}; ", list[i].DbColumnName));
                        sb2.AppendLine(string.Format("            mo.{0}=F_{0}.SelectedDate; ", list[i].DbColumnName));
                        break;
                    case "bigint":
                        sb.AppendLine(string.Format("                        F_{0}.Text = mo.{0}.ToString(); ", list[i].DbColumnName));
                        sb2.AppendLine(string.Format("            mo.{0}= Convert.ToInt64(F_{0}.Text); ", list[i].DbColumnName));

                        if (list[i].DefaultValue.Length == 0)//默认值验证
                        {
                            sb3.AppendLine(string.Format("try {{ Convert.ToInt64(F_{0}.Text); }} catch {{ NotifyWarning(\"请为“{1}”填写有效值！\"); return; }}", list[i].DbColumnName, GetComment(list[i].ColumnDescription, list[i].DbColumnName)));
                        }

                        break;
                    case "smallint"://break;
                    case "tinyint"://break;
                    case "int":
                        sb.AppendLine(string.Format("                        F_{0}.Text = mo.{0}.ToString(); ", list[i].DbColumnName));
                        sb2.AppendLine(string.Format("            mo.{0}= int.Parse(F_{0}.Text); ", list[i].DbColumnName));

                        if (list[i].DefaultValue.Length == 0)//默认值验证
                        {
                            sb3.AppendLine(string.Format("try {{ int.Parse(F_{0}.Text); }} catch {{ NotifyWarning(\"请为“{1}”填写有效值！\"); return; }}", list[i].DbColumnName, GetComment(list[i].ColumnDescription, list[i].DbColumnName)));
                        }

                        break;

                    case "money":// break;
                    case "decimal":
                        sb.AppendLine(string.Format("                        F_{0}.Text = mo.{0}.ToString(); ", list[i].DbColumnName));
                        sb2.AppendLine(string.Format("            mo.{0}= decimal.Parse(F_{0}.Text); ", list[i].DbColumnName));

                        if (list[i].DefaultValue.Length == 0)//默认值验证
                        {
                            sb3.AppendLine(string.Format("try {{ decimal.Parse(F_{0}.Text); }} catch {{ NotifyWarning(\"请为“{1}”填写有效值！\"); return; }}", list[i].DbColumnName, GetComment(list[i].ColumnDescription, list[i].DbColumnName)));
                        }

                        break;

                    case "float":
                        sb.AppendLine(string.Format("                        F_{0}.Text = mo.{0}.ToString(); ", list[i].DbColumnName));
                        sb2.AppendLine(string.Format("            mo.{0}= double.Parse(F_{0}.Text); ", list[i].DbColumnName));

                        if (list[i].DefaultValue.Length == 0)//默认值验证
                        {
                            sb3.AppendLine(string.Format("try {{ double.Parse(F_{0}.Text); }} catch {{ NotifyWarning(\"请为“{1}”填写有效值！\"); return; }}", list[i].DbColumnName, GetComment(list[i].ColumnDescription, list[i].DbColumnName)));
                        }

                        break;
                    default:
                        sb.AppendLine(string.Format("                        F_{0}.Text = mo.{0}; ", list[i].DbColumnName));
                        sb2.AppendLine(string.Format("            mo.{0}= F_{0}.Text; ", list[i].DbColumnName));
                        break;
                }

                sb4.Append(",p." + list[i].DbColumnName);
            }

            string updateColumns = "p => new {" + sb4.ToString().Substring(1) + "}";
            var keyModel = tb.Columns.FirstOrDefault(p => p.IsPrimarykey == true);

            if (keyModel == null) { throw new Exception("表没有主键，没办法生成代码！FineuiAdd_cs"); }

            string code = string.Format(@"
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SqlSugar ;
using FineUIPro;

namespace {0}.{1}{2}
{{
    public partial class Add : {3}
    {{  
        protected void Page_Load(object sender, EventArgs e)
        {{
            if (!IsPostBack)
            {{               

                if (Request[""id""] != null) //修改
                {{
                    try
                    {{
                        {9}.{2} mo = DBServices.DB_Base.GetModel<{9}.{2}>(int.Parse(Request[""id""]));       
{4}                    
                    }}
                    catch (Exception ex)
                    {{
                        Response.End();
                    }}
                }}
            }}
        }}

        protected void Button_save_OnClick(object sender, EventArgs e)
        {{            
            //验证
            {6}

            {9}.{2} mo=new {9}.{2}();
            {5}

            if (Request[""id""] == null)//添加
            {{
                if (DBServices.DB_Base.InsertModel(mo) > 0)
                {{
                    AlertInfor(""添加成功"", true);
                }}
                else
                {{
                    NotifyError(""添加失败"");
                }}

            }}
            else//修改
            {{
                 int returnNum= DBServices.DB_Base.UpdateModels(mo, 
                           //注意更新字段是否对应，数量是否一致 
{7},
                           //条件
                           p => p.{8}== int.Parse(Request[""id""]));
                if (returnNum>0)
                {{
                    AlertInfor(""修改成功"", true);
                }}
                else
                {{
                    NotifyError(""修改失败"");
                }}
            }}
        }}
    }}
}}

", tb.NamespaceStr,
   GetNamespace2Str(tb.Namespace2Str),
   tb.TableName,
   tb.ClassnameStr,
   sb.ToString(),
   sb2.ToString(),
   sb3.ToString(),
   updateColumns,
   keyModel.DbColumnName,
   tb.ModelName);


            return code;
        }


    }
}
