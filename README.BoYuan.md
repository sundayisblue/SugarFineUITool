## 0. 介绍BoYuan快速开发框架。
实现页面权限或页面+button权限的后台框架，并有完善的异常拦截写入日志功能。
项目为webform开发模式，fineuipro + sqlsugar，简单的service分层架构。
配有代码生成工具和其他利于编程的工具，优秀的编码体验，层次分明，简单易学，从而实现快速开发的目的，适用于中小型项目开发。
建议使用vs2017以上版本开发工具 ， mssql数据库 ，Framework 4.5 以上版本。
有建议或问题，欢迎加我QQ:971131282

免费开源的简化版BoYuan框架:SugarFineUI
> [https://gitee.com/sundayisblue/SugarFineUITool](https://gitee.com/sundayisblue/SugarFineUITool)


## 1. 项目初始化，并创建数据库。 
打开项目，如图会看到项目基本结构。也可以参看项目说明文档，会有**项目概述**。
![项目概述.png](https://images.gitee.com/uploads/images/2020/0204/153149_c717ffb7_436641.png)


让我们先从数据库创建开始，也就是DBFirst开发模式。如下图，执行 **添加后台权限表.sql**，当然你需要自己先创建一个数据库，比如说mydddd数据库(mydddd只是做演示)

![执行初始化数据库脚本.png](https://images.gitee.com/uploads/images/2020/0204/153150_1fd4a9ff_436641.png)


![生成的默认表.png](https://images.gitee.com/uploads/images/2020/0204/153150_cffe16fb_436641.png)

创建我们的业务表，这里创建**员工表**和**部门表**。默认都是以id作为自增主键。
![员工表.png](https://images.gitee.com/uploads/images/2020/0204/153149_02b7fb41_436641.png)

![部门表.png](https://images.gitee.com/uploads/images/2020/0204/153149_4e8deb6d_436641.png)

## 2. 生成Entity实体模型和FineuiPro页面代码。
创建好数据库后，我们就可以执行代码工具。
![执行代码工具页面.png](https://images.gitee.com/uploads/images/2020/0204/153150_27ce04e4_436641.png)

首先 生成SqlSugar实体，选择对应的页面。如下图，点击连接数据库，暂时不支持单表实体生成。默认是放到C盘目录下，个人不喜欢直接覆盖到项目代码，当然如果你喜欢可以修改。
![连接数据库生成实体模型.png](https://images.gitee.com/uploads/images/2020/0204/153150_b6989620_436641.png)

把生成的实体放入到entity层
![生成的实体放入到entity层.png](https://images.gitee.com/uploads/images/2020/0204/153150_6878e761_436641.png)

生成FineUIPro页面。 选择FineUIPro页面，先在右边连接好数据库，选择要生成页面的相关表，代码生成是根据单表结构来生成的；生成代码相关信息，比如说命名空间、路径等，默认不需要修改；生成的代码默认放在C盘，这里我只想生成编辑页面，列表页面，单选即可，最后点击**生成简单代码**按钮，生成相关页面。
![生成页面设置.png](https://images.gitee.com/uploads/images/2020/0204/153150_984332d8_436641.png)

把生成页面代码放入到项目中，先别着急运行项目：选中项目->vs菜单**项目**->执行**转换为Web应用程序**。
![把生成页面代码放入到项目中.png](https://images.gitee.com/uploads/images/2020/0204/153150_1f71faa0_436641.png)

![转换为Web应用程序](https://images.gitee.com/uploads/images/2020/0204/153149_b875cc29_436641.png)

如果想查看单页面生成代码，选择表(多选的表还是默认选择选中的第一个表)，点击按钮**显示单页代码**即可。
![单页代码显示](https://images.gitee.com/uploads/images/2020/0204/153149_40635eaa_436641.png)

至此项目代码生成完毕，重新编译生成项目，保证项目不报错。


## 3. 权限配置。
运行项目BoYuan.FineuiProWeb。在login.aspx页面里，我为了方便调试，page_load事件里增加了自动登录的代码，项目发布时候不要忘记删除。
![默认登录](https://images.gitee.com/uploads/images/2020/0204/153150_519d2a95_436641.png)

登录后默认能看到相关的管理员设置。如果想显示新增加的页面到系统里，在**页面组件**里设置。
![添加页面组件](https://images.gitee.com/uploads/images/2020/0204/153150_74a38994_436641.png)

右键点击根节点，可以添加根页面；
![右键添加页面](https://images.gitee.com/uploads/images/2020/0204/153150_b06e68d1_436641.png)

如下图，左边点击可以设置页面所在路径。右边**URL地址**,可以设置url地址，也可以是外网地址(需要填写完整的url，例:https:// www .baidu .com)；如果是文件夹，可以不填写。**状态显示** 是在左侧菜单里是否显示状态。
![添加页面组件](https://images.gitee.com/uploads/images/2020/0204/153150_113c5897_436641.png)

回头，我们看下代码生成工具，批量添加页面组件。如果生成的页面比较多，手动一个个添加是个麻烦的体力活儿。当然我也想偷懒，让Boyuan框架帮你去做。一键添加页面组件到数据库里，如下图。会根据你项目所有的aspx文件，同步到数据里。(同理，可以同步button权限，这里暂时不讨论button权限。)
![一键添加页面组件到数据库](https://images.gitee.com/uploads/images/2020/0204/153150_88cb32c1_436641.png)



我们在回来，在系统中查看一下，同步的页面组件数据。当然 页面名称需要自己修改，如下图。
![同步后页面组件数据](https://images.gitee.com/uploads/images/2020/0204/153150_39a5b2d0_436641.png)

设置权限角色。如下图，更改超级管理员角色权限，让新增的页面显示出来。打挑的都是有权访问的页面。
![设置角色的权限](https://images.gitee.com/uploads/images/2020/0204/153150_d6c6c693_436641.png)

设置好角色的权限后，需要点击**更新缓存**，刷新下页面，就能看到新增加的页面。但有时也不好用，重新登录下即可。
![更新缓存](https://images.gitee.com/uploads/images/2020/0204/153150_b28c4066_436641.png)

查看生成的页面，可以CRUD数据了，但是离你的业务还差一点，这需要手动编码了。
![查看生成的页面](https://images.gitee.com/uploads/images/2020/0204/153150_4e649477_436641.png)

## 4. 完善业务代码。

1. **部门管理** 模块。添加或编辑页面基本可以不动。**部门列表页面**的需要，添加关键词搜索，伪删除功能实现。

**部门列表页面** 关键词代码实现:红框里代表修改的代码，获取关键词，模糊查询部门名称。
![关键词代码实现](https://images.gitee.com/uploads/images/2020/0204/153150_26033088_436641.png)

**部门列表页面** 伪删除功能: 默认是批量删除代码，需要修改成伪删除代码。如下 2个图。
![默认删除方法](https://images.gitee.com/uploads/images/2020/0204/153150_f38f2f0f_436641.png)

![修改后的伪删除方法](https://images.gitee.com/uploads/images/2020/0204/153150_f3fc93e5_436641.png)

2. **员工管理** 模块。**员工管理**涉及到多表连接查询，添加一个员工信息同时需要添加所在部门，这里我们用下拉框来表示部门。


**员工管理** 列表页面，需要添加**性别**下拉框，**部门**下拉框，员工姓名或员工编码模糊查询。**性别**字段，这里我打算用枚举对象来实现，我们先创建个**性别**枚举。
![性别枚举](https://images.gitee.com/uploads/images/2020/0204/153150_a3fbdbb2_436641.png)

列表页面的初始化方法，增加**性别**：
![性别aspx](https://images.gitee.com/uploads/images/2020/0204/153150_46163342_436641.png)

![性别cs](https://images.gitee.com/uploads/images/2020/0204/153150_50c42cfe_436641.png)

列表页面的初始化方法，增加**部门**：
![部门aspx](https://images.gitee.com/uploads/images/2020/0204/153150_40e7a37d_436641.png)

![部门aspx列表字段](https://images.gitee.com/uploads/images/2020/0204/153150_a832c7d1_436641.png)

![部门cs](https://images.gitee.com/uploads/images/2020/0204/153150_1b357f08_436641.png)

列表页面的绑定方法 bind:

先做关联查询。如下图，写select字段比较麻烦，如果字段太多是个很不爽的体力活。BoYuan框架有解决方案。
![SqlSugar左查询](https://images.gitee.com/uploads/images/2020/0204/153150_c2db87e0_436641.png)

选择代码工具，反射获取字段代码页面。如下图，操作获取员工相关字段信息。
![反射获取员工实体字段信息](https://images.gitee.com/uploads/images/2020/0204/153150_c33a6166_436641.png)

同理，可以获取部门字段信息。
![反射获取部门实体字段信息](https://images.gitee.com/uploads/images/2020/0204/153150_bf9749c9_436641.png)

最后把，上面操作的2段代码进行复制到项目里，并合并。
![合并后的Select代码](https://images.gitee.com/uploads/images/2020/0204/153150_745f23cc_436641.png)

动态添加查询条件。列表页面所有的业务逻辑完成。
![动态添加查询条件](https://images.gitee.com/uploads/images/2020/0204/153150_bef28d79_436641.png)


**员工管理**  编辑页面，业务代码完善。如下图，需要修改**性别** 和 **所在部门** 为下拉框。
![待完善的编辑页面](https://images.gitee.com/uploads/images/2020/0204/153150_07a2b634_436641.png)

修改编辑页面：
![编辑页面aspx](https://images.gitee.com/uploads/images/2020/0204/153150_fffc16eb_436641.png)

![后台page_Load事件](https://images.gitee.com/uploads/images/2020/0204/153150_3afc0153_436641.png)

![后台button事件](https://images.gitee.com/uploads/images/2020/0204/153150_508775c3_436641.png)

至此，所有的业务代码完成。自己添加数据试试效果吧！